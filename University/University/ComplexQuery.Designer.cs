﻿namespace University
{
    partial class ComplexQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseUI = new System.Windows.Forms.Button();
            this.ExportResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.query = new System.Windows.Forms.TextBox();
            this.runQuery = new System.Windows.Forms.Button();
            this.ComplexDGV = new System.Windows.Forms.DataGridView();
            this.Status = new System.Windows.Forms.Label();
            this.RelationshipImage = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComplexDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelationshipImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.CloseUI);
            this.panel1.Controls.Add(this.ExportResult);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Perpetua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1352, 66);
            this.panel1.TabIndex = 7;
            // 
            // CloseUI
            // 
            this.CloseUI.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseUI.Location = new System.Drawing.Point(1140, 11);
            this.CloseUI.Name = "CloseUI";
            this.CloseUI.Size = new System.Drawing.Size(141, 48);
            this.CloseUI.TabIndex = 2;
            this.CloseUI.Text = "Close Window";
            this.CloseUI.UseVisualStyleBackColor = true;
            this.CloseUI.Click += new System.EventHandler(this.CloseUI_Click);
            // 
            // ExportResult
            // 
            this.ExportResult.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportResult.Location = new System.Drawing.Point(809, 11);
            this.ExportResult.Name = "ExportResult";
            this.ExportResult.Size = new System.Drawing.Size(141, 48);
            this.ExportResult.TabIndex = 1;
            this.ExportResult.Text = "Export Results";
            this.ExportResult.UseVisualStyleBackColor = true;
            this.ExportResult.Click += new System.EventHandler(this.ExportResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "University Management Kit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(78, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Write your query here";
            // 
            // query
            // 
            this.query.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.query.Location = new System.Drawing.Point(278, 88);
            this.query.Multiline = true;
            this.query.Name = "query";
            this.query.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.query.Size = new System.Drawing.Size(828, 78);
            this.query.TabIndex = 3;
            // 
            // runQuery
            // 
            this.runQuery.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runQuery.Location = new System.Drawing.Point(1141, 88);
            this.runQuery.Name = "runQuery";
            this.runQuery.Size = new System.Drawing.Size(141, 48);
            this.runQuery.TabIndex = 5;
            this.runQuery.Text = "Execute";
            this.runQuery.UseVisualStyleBackColor = true;
            this.runQuery.Click += new System.EventHandler(this.runQuery_Click);
            // 
            // ComplexDGV
            // 
            this.ComplexDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ComplexDGV.Location = new System.Drawing.Point(39, 180);
            this.ComplexDGV.Name = "ComplexDGV";
            this.ComplexDGV.Size = new System.Drawing.Size(899, 652);
            this.ComplexDGV.TabIndex = 11;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(915, 169);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 20);
            this.Status.TabIndex = 13;
            // 
            // RelationshipImage
            // 
            this.RelationshipImage.BackColor = System.Drawing.SystemColors.Window;
            this.RelationshipImage.Image = global::University.Properties.Resources._2018_09_23_16h36_03;
            this.RelationshipImage.Location = new System.Drawing.Point(946, 180);
            this.RelationshipImage.Name = "RelationshipImage";
            this.RelationshipImage.Size = new System.Drawing.Size(616, 652);
            this.RelationshipImage.TabIndex = 12;
            this.RelationshipImage.TabStop = false;
            // 
            // ComplexQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1590, 844);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.RelationshipImage);
            this.Controls.Add(this.ComplexDGV);
            this.Controls.Add(this.runQuery);
            this.Controls.Add(this.query);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Name = "ComplexQuery";
            this.Text = "ComplexQuery";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComplexDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelationshipImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseUI;
        private System.Windows.Forms.Button ExportResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox query;
        private System.Windows.Forms.Button runQuery;
        private System.Windows.Forms.DataGridView ComplexDGV;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.PictureBox RelationshipImage;
    }
}