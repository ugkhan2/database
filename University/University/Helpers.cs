﻿using System;
using System.Windows.Forms;
using System.Data;

namespace University
{
    class Helpers
    {
        private SqlDbConnect con;

        public Helpers()
        {
            con = new SqlDbConnect();

        }

        /**
         *
         * Asks user permission whether to delete the item or not.
         *          
         */
        public Boolean DeleteAlertBox()
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??", "Confirm Delete!!", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    #region CREATE EXCEL FILE
        private void copyAlltoClipboard(DataGridView dgv)
        {
            dgv.MultiSelect = true;
            dgv.SelectAll();
            DataObject dataObj = dgv.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        public void ExportResults(DataGridView dgv)
        {
            copyAlltoClipboard(dgv);
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            dgv.MultiSelect = false;
        }
    #endregion create_the_Excel_file

    #region POPULATE_COMBOBOX
        public void PopulateSelectBoxes(ComboBox selectBox, string queryString)
        {
            con.SqlQuery(queryString);
            selectBox.Items.Clear();
            foreach (DataRow dr in con.ExecuteReturnQuery().Rows)
            {
                selectBox.Items.Add(new ComboBoxItem(dr[1].ToString().Trim(), dr[0].ToString().Trim()));
            }
            selectBox.SelectedIndex = selectBox.Items.Count - 1;
        }
        #endregion

        #region SHOW STATUS MESSAGE
        public void ShowQueryStatus(Label label, string message, string color = "") {
            label.Text = message;
            if (color == "Err") {
                label.ForeColor = System.Drawing.Color.Red;
            } else
            {
                label.ForeColor = System.Drawing.Color.Green;
            }
        }
    #endregion
    }
}
