﻿namespace University
{
    partial class LecturesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LecturesDGV = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lecDuration = new System.Windows.Forms.TextBox();
            this.lecUpdate = new System.Windows.Forms.Button();
            this.lecDel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.queryStatus = new System.Windows.Forms.Label();
            this.lecNew = new System.Windows.Forms.Button();
            this.SubjectName = new System.Windows.Forms.TextBox();
            this.lecDate = new System.Windows.Forms.TextBox();
            this.formLecID = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseUI = new System.Windows.Forms.Button();
            this.ExportResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Lec_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LecturesDGV)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LecturesDGV
            // 
            this.LecturesDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.LecturesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LecturesDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Lec_ID,
            this.Subject,
            this.Date,
            this.Duration});
            this.LecturesDGV.Location = new System.Drawing.Point(71, 138);
            this.LecturesDGV.Name = "LecturesDGV";
            this.LecturesDGV.Size = new System.Drawing.Size(492, 486);
            this.LecturesDGV.TabIndex = 9;
            this.LecturesDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.LecturesDGV_CellMouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lecDuration);
            this.groupBox1.Controls.Add(this.lecUpdate);
            this.groupBox1.Controls.Add(this.lecDel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.queryStatus);
            this.groupBox1.Controls.Add(this.lecNew);
            this.groupBox1.Controls.Add(this.SubjectName);
            this.groupBox1.Controls.Add(this.lecDate);
            this.groupBox1.Controls.Add(this.formLecID);
            this.groupBox1.Location = new System.Drawing.Point(673, 138);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(641, 486);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Duration";
            // 
            // lecDuration
            // 
            this.lecDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecDuration.Location = new System.Drawing.Point(243, 213);
            this.lecDuration.Name = "lecDuration";
            this.lecDuration.Size = new System.Drawing.Size(336, 26);
            this.lecDuration.TabIndex = 5;
            // 
            // lecUpdate
            // 
            this.lecUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecUpdate.Location = new System.Drawing.Point(218, 363);
            this.lecUpdate.Name = "lecUpdate";
            this.lecUpdate.Size = new System.Drawing.Size(145, 55);
            this.lecUpdate.TabIndex = 7;
            this.lecUpdate.Text = "Update";
            this.lecUpdate.UseVisualStyleBackColor = true;
            this.lecUpdate.Click += new System.EventHandler(this.lecUpdate_Click);
            // 
            // lecDel
            // 
            this.lecDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecDel.Location = new System.Drawing.Point(405, 363);
            this.lecDel.Name = "lecDel";
            this.lecDel.Size = new System.Drawing.Size(145, 55);
            this.lecDel.TabIndex = 8;
            this.lecDel.Text = "Delete";
            this.lecDel.UseVisualStyleBackColor = true;
            this.lecDel.Click += new System.EventHandler(this.lecDel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Subject";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(41, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date";
            // 
            // queryStatus
            // 
            this.queryStatus.AutoSize = true;
            this.queryStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.queryStatus.Location = new System.Drawing.Point(61, 26);
            this.queryStatus.Name = "queryStatus";
            this.queryStatus.Size = new System.Drawing.Size(0, 20);
            this.queryStatus.TabIndex = 4;
            // 
            // lecNew
            // 
            this.lecNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecNew.Location = new System.Drawing.Point(36, 363);
            this.lecNew.Name = "lecNew";
            this.lecNew.Size = new System.Drawing.Size(145, 55);
            this.lecNew.TabIndex = 6;
            this.lecNew.Text = "Add";
            this.lecNew.UseVisualStyleBackColor = true;
            this.lecNew.Click += new System.EventHandler(this.lecNew_Click);
            // 
            // SubjectName
            // 
            this.SubjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubjectName.Location = new System.Drawing.Point(243, 123);
            this.SubjectName.Name = "SubjectName";
            this.SubjectName.Size = new System.Drawing.Size(336, 26);
            this.SubjectName.TabIndex = 3;
            // 
            // lecDate
            // 
            this.lecDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecDate.Location = new System.Drawing.Point(243, 166);
            this.lecDate.Name = "lecDate";
            this.lecDate.Size = new System.Drawing.Size(336, 26);
            this.lecDate.TabIndex = 4;
            // 
            // formLecID
            // 
            this.formLecID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formLecID.Location = new System.Drawing.Point(243, 80);
            this.formLecID.Name = "formLecID";
            this.formLecID.Size = new System.Drawing.Size(336, 26);
            this.formLecID.TabIndex = 0;
            this.formLecID.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(71, 138);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(492, 486);
            this.dataGridView1.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.CloseUI);
            this.panel1.Controls.Add(this.ExportResult);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Perpetua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1349, 66);
            this.panel1.TabIndex = 6;
            // 
            // CloseUI
            // 
            this.CloseUI.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseUI.Location = new System.Drawing.Point(1140, 11);
            this.CloseUI.Name = "CloseUI";
            this.CloseUI.Size = new System.Drawing.Size(141, 48);
            this.CloseUI.TabIndex = 2;
            this.CloseUI.Text = "Close Window";
            this.CloseUI.UseVisualStyleBackColor = true;
            this.CloseUI.Click += new System.EventHandler(this.CloseUI_Click);
            // 
            // ExportResult
            // 
            this.ExportResult.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportResult.Location = new System.Drawing.Point(809, 11);
            this.ExportResult.Name = "ExportResult";
            this.ExportResult.Size = new System.Drawing.Size(141, 48);
            this.ExportResult.TabIndex = 1;
            this.ExportResult.Text = "Export Results";
            this.ExportResult.UseVisualStyleBackColor = true;
            this.ExportResult.Click += new System.EventHandler(this.ExportResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "University Management Kit";
            // 
            // Lec_ID
            // 
            this.Lec_ID.DataPropertyName = "Lec_ID";
            this.Lec_ID.HeaderText = "Lec_ID";
            this.Lec_ID.Name = "Lec_ID";
            this.Lec_ID.Visible = false;
            // 
            // Subject
            // 
            this.Subject.DataPropertyName = "Subject";
            this.Subject.HeaderText = "Subject";
            this.Subject.Name = "Subject";
            // 
            // Date
            // 
            this.Date.DataPropertyName = "Date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            // 
            // Duration
            // 
            this.Duration.DataPropertyName = "Duration";
            this.Duration.HeaderText = "Duration";
            this.Duration.Name = "Duration";
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(25, 32);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 24);
            this.Status.TabIndex = 11;
            // 
            // LecturesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 642);
            this.Controls.Add(this.LecturesDGV);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Name = "LecturesForm";
            this.Text = "LecturesForm";
            ((System.ComponentModel.ISupportInitialize)(this.LecturesDGV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView LecturesDGV;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button lecUpdate;
        private System.Windows.Forms.Button lecDel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label queryStatus;
        private System.Windows.Forms.Button lecNew;
        private System.Windows.Forms.TextBox SubjectName;
        private System.Windows.Forms.TextBox lecDate;
        private System.Windows.Forms.TextBox formLecID;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseUI;
        private System.Windows.Forms.Button ExportResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lecDuration;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lec_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
        private System.Windows.Forms.Label Status;
    }
}