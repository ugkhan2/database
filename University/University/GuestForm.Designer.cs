﻿namespace University
{
    partial class GuestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseUI = new System.Windows.Forms.Button();
            this.ExportResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Status = new System.Windows.Forms.Label();
            this.datenTime = new System.Windows.Forms.TextBox();
            this.UpdateGuest = new System.Windows.Forms.Button();
            this.DeleteGuest = new System.Windows.Forms.Button();
            this.AddGuest = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lable = new System.Windows.Forms.Label();
            this.guestID = new System.Windows.Forms.TextBox();
            this.duration = new System.Windows.Forms.TextBox();
            this.lecID = new System.Windows.Forms.ComboBox();
            this.lectID = new System.Windows.Forms.ComboBox();
            this.orgID = new System.Windows.Forms.ComboBox();
            this.GuestDGV = new System.Windows.Forms.DataGridView();
            this.Guest_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Org_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lect_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GuestDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.CloseUI);
            this.panel1.Controls.Add(this.ExportResult);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Perpetua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(-27, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1349, 66);
            this.panel1.TabIndex = 5;
            // 
            // CloseUI
            // 
            this.CloseUI.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseUI.Location = new System.Drawing.Point(1140, 11);
            this.CloseUI.Name = "CloseUI";
            this.CloseUI.Size = new System.Drawing.Size(141, 48);
            this.CloseUI.TabIndex = 2;
            this.CloseUI.Text = "Close Window";
            this.CloseUI.UseVisualStyleBackColor = true;
            this.CloseUI.Click += new System.EventHandler(this.CloseUI_Click);
            // 
            // ExportResult
            // 
            this.ExportResult.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportResult.Location = new System.Drawing.Point(809, 11);
            this.ExportResult.Name = "ExportResult";
            this.ExportResult.Size = new System.Drawing.Size(141, 48);
            this.ExportResult.TabIndex = 1;
            this.ExportResult.Text = "Export Results";
            this.ExportResult.UseVisualStyleBackColor = true;
            this.ExportResult.Click += new System.EventHandler(this.ExportResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "University Management Kit";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.datenTime);
            this.groupBox1.Controls.Add(this.UpdateGuest);
            this.groupBox1.Controls.Add(this.DeleteGuest);
            this.groupBox1.Controls.Add(this.AddGuest);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lable);
            this.groupBox1.Controls.Add(this.guestID);
            this.groupBox1.Controls.Add(this.duration);
            this.groupBox1.Controls.Add(this.lecID);
            this.groupBox1.Controls.Add(this.lectID);
            this.groupBox1.Controls.Add(this.orgID);
            this.groupBox1.Location = new System.Drawing.Point(685, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(624, 560);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(98, 20);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 24);
            this.Status.TabIndex = 14;
            // 
            // datenTime
            // 
            this.datenTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datenTime.Location = new System.Drawing.Point(213, 305);
            this.datenTime.Name = "datenTime";
            this.datenTime.Size = new System.Drawing.Size(356, 26);
            this.datenTime.TabIndex = 6;
            // 
            // UpdateGuest
            // 
            this.UpdateGuest.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateGuest.Location = new System.Drawing.Point(289, 423);
            this.UpdateGuest.Name = "UpdateGuest";
            this.UpdateGuest.Size = new System.Drawing.Size(141, 48);
            this.UpdateGuest.TabIndex = 9;
            this.UpdateGuest.Text = "Update";
            this.UpdateGuest.UseVisualStyleBackColor = true;
            this.UpdateGuest.Click += new System.EventHandler(this.UpdateGuest_Click);
            // 
            // DeleteGuest
            // 
            this.DeleteGuest.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteGuest.Location = new System.Drawing.Point(467, 423);
            this.DeleteGuest.Name = "DeleteGuest";
            this.DeleteGuest.Size = new System.Drawing.Size(141, 48);
            this.DeleteGuest.TabIndex = 11;
            this.DeleteGuest.Text = "Delete";
            this.DeleteGuest.UseVisualStyleBackColor = true;
            this.DeleteGuest.Click += new System.EventHandler(this.DeleteGuest_Click);
            // 
            // AddGuest
            // 
            this.AddGuest.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddGuest.Location = new System.Drawing.Point(111, 423);
            this.AddGuest.Name = "AddGuest";
            this.AddGuest.Size = new System.Drawing.Size(141, 48);
            this.AddGuest.TabIndex = 8;
            this.AddGuest.Text = "Add";
            this.AddGuest.UseVisualStyleBackColor = true;
            this.AddGuest.Click += new System.EventHandler(this.AddGuest_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(94, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 20);
            this.label9.TabIndex = 10;
            this.label9.Text = "Subject";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(94, 255);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Lecturer";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(94, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "Date and Time";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(94, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Duration";
            // 
            // lable
            // 
            this.lable.AutoSize = true;
            this.lable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable.Location = new System.Drawing.Point(94, 156);
            this.lable.Name = "lable";
            this.lable.Size = new System.Drawing.Size(99, 20);
            this.lable.TabIndex = 6;
            this.lable.Text = "Organization";
            // 
            // guestID
            // 
            this.guestID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guestID.Location = new System.Drawing.Point(215, 99);
            this.guestID.Name = "guestID";
            this.guestID.Size = new System.Drawing.Size(356, 26);
            this.guestID.TabIndex = 5;
            this.guestID.Visible = false;
            // 
            // duration
            // 
            this.duration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.duration.Location = new System.Drawing.Point(215, 354);
            this.duration.Name = "duration";
            this.duration.Size = new System.Drawing.Size(356, 26);
            this.duration.TabIndex = 7;
            // 
            // lecID
            // 
            this.lecID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lecID.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lecID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecID.FormattingEnabled = true;
            this.lecID.Location = new System.Drawing.Point(215, 195);
            this.lecID.Name = "lecID";
            this.lecID.Size = new System.Drawing.Size(356, 28);
            this.lecID.TabIndex = 4;
            // 
            // lectID
            // 
            this.lectID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lectID.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lectID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lectID.FormattingEnabled = true;
            this.lectID.Location = new System.Drawing.Point(215, 247);
            this.lectID.Name = "lectID";
            this.lectID.Size = new System.Drawing.Size(356, 28);
            this.lectID.TabIndex = 5;
            // 
            // orgID
            // 
            this.orgID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.orgID.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.orgID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orgID.FormattingEnabled = true;
            this.orgID.Location = new System.Drawing.Point(215, 148);
            this.orgID.Name = "orgID";
            this.orgID.Size = new System.Drawing.Size(356, 28);
            this.orgID.TabIndex = 3;
            // 
            // GuestDGV
            // 
            this.GuestDGV.AllowUserToAddRows = false;
            this.GuestDGV.AllowUserToDeleteRows = false;
            this.GuestDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GuestDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Guest_ID,
            this.Org_name,
            this.Subject,
            this.Lect_name,
            this.Date,
            this.Time});
            this.GuestDGV.Location = new System.Drawing.Point(12, 76);
            this.GuestDGV.Name = "GuestDGV";
            this.GuestDGV.ReadOnly = true;
            this.GuestDGV.Size = new System.Drawing.Size(658, 560);
            this.GuestDGV.TabIndex = 7;
            this.GuestDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GuestDGV_CellMouseClick);
            // 
            // Guest_ID
            // 
            this.Guest_ID.DataPropertyName = "Guest_ID";
            this.Guest_ID.HeaderText = "GuestID";
            this.Guest_ID.Name = "Guest_ID";
            this.Guest_ID.ReadOnly = true;
            this.Guest_ID.Visible = false;
            // 
            // Org_name
            // 
            this.Org_name.DataPropertyName = "Org_name";
            this.Org_name.HeaderText = "Organization Name";
            this.Org_name.Name = "Org_name";
            this.Org_name.ReadOnly = true;
            // 
            // Subject
            // 
            this.Subject.DataPropertyName = "Subject";
            this.Subject.HeaderText = "Subject";
            this.Subject.Name = "Subject";
            this.Subject.ReadOnly = true;
            // 
            // Lect_name
            // 
            this.Lect_name.DataPropertyName = "Lect_name";
            this.Lect_name.HeaderText = "Lecturer";
            this.Lect_name.Name = "Lect_name";
            this.Lect_name.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "Date";
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Time
            // 
            this.Time.DataPropertyName = "Time";
            this.Time.HeaderText = "Duration";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            // 
            // GuestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1321, 648);
            this.Controls.Add(this.GuestDGV);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "GuestForm";
            this.Text = "GuestForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GuestDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseUI;
        private System.Windows.Forms.Button ExportResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button UpdateGuest;
        private System.Windows.Forms.Button DeleteGuest;
        private System.Windows.Forms.Button AddGuest;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lable;
        private System.Windows.Forms.TextBox guestID;
        private System.Windows.Forms.TextBox duration;
        private System.Windows.Forms.ComboBox lecID;
        private System.Windows.Forms.ComboBox lectID;
        private System.Windows.Forms.ComboBox orgID;
        private System.Windows.Forms.DataGridView GuestDGV;
        private System.Windows.Forms.TextBox datenTime;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Guest_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Org_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lect_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
    }
}