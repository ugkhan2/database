﻿using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace University
{
    public class SqlDbConnect {
        private SqlConnection _con;
        public SqlCommand Cmd;
        private SqlDataAdapter _da;
        private DataTable _dt;

        public SqlDbConnect() {
            _con = new SqlConnection("Data Source=DESKTOP-L74PRHK;Initial Catalog=University;Integrated Security=True");
            _con.Open();
        }

        public void SqlQuery(string queryText) {
            Cmd = new SqlCommand(queryText, _con);
        }

        public DataTable ExecuteReturnQuery() {
            _da = new SqlDataAdapter(Cmd);
            _dt = new DataTable();
            _da.Fill(_dt);
            return _dt;
        }

        public void ExecuteCRUDQuery() {
            Cmd.ExecuteNonQuery();
        }
    }
}
