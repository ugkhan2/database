﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace University
{
    public partial class OrganizationForm : Form
    {

        Helpers helpers = new Helpers();
        private SqlDbConnect con;
        public OrganizationForm()
        {
            con = new SqlDbConnect();
            InitializeComponent();
            UpdateUI();
        }

        private void UpdateUI()
        {
            OrgDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            OrgDGV.MultiSelect = false;
            con.SqlQuery("SELECT * FROM Organization");
            var Guests = con.ExecuteReturnQuery();
            OrgDGV.AutoGenerateColumns = false;
            OrgDGV.DataSource = Guests;
            OrgDGV.ClearSelection();
        }

        private void CloseUI_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ExportResult_Click(object sender, EventArgs e)
        {
            helpers.ExportResults(OrgDGV);
        }

        private void OrgDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.OrgDGV.Rows[e.RowIndex];
                formOrgID.Text = row.Cells["Org_ID"].Value.ToString().Trim();
                orgName.Text = row.Cells["Org_name"].Value.ToString().Trim();
                orgAdd.Text = row.Cells["Address"].Value.ToString().Trim();
            }
        }

        private void orgNew_Click(object sender, EventArgs e)
        {
            string OrgName = orgName.Text.Trim();
            string orgAddress = orgAdd.Text.Trim();

            if (!string.IsNullOrEmpty(OrgName) && !string.IsNullOrEmpty(orgAddress))
            {
                con.SqlQuery("INSERT INTO Organization (Org_name, Address) VALUES (@name, @address)");
                con.Cmd.Parameters.AddWithValue("@name", OrgName);
                con.Cmd.Parameters.AddWithValue("@address", orgAddress);
                con.ExecuteCRUDQuery();
                UpdateUI();
                orgName.Text = "";
                orgAdd.Text = "";
                helpers.ShowQueryStatus(Status, "Data saved");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void orgUpdate_Click(object sender, EventArgs e)
        {
            string OrgName = orgName.Text.Trim();
            string orgID = formOrgID.Text.Trim();
            string orgAddress = orgAdd.Text.Trim();

            if (!string.IsNullOrEmpty(OrgName) && !string.IsNullOrEmpty(orgAddress))
            {
                con.SqlQuery("UPDATE Organization SET Org_name = @name, Address = @address WHERE Org_ID = @ID");
                con.Cmd.Parameters.AddWithValue("@name", OrgName);
                con.Cmd.Parameters.AddWithValue("@address", orgAddress);
                con.Cmd.Parameters.AddWithValue("@ID", orgID);
                con.ExecuteCRUDQuery();
                UpdateUI();
                orgName.Text = "";
                orgAdd.Text = "";
                helpers.ShowQueryStatus(Status, "Data Updated");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void orgDel_Click(object sender, EventArgs e)
        {
            if (OrgDGV.SelectedRows.Count > 0)
            {
                var confirmResult = MessageBox.Show("Are you sure to delete this item ??", "Confirm Delete!!", MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    int ID = Int32.Parse(formOrgID.Text.Trim());
                    try
                    {
                        con.SqlQuery("DELETE FROM Organization WHERE Org_ID = @ID");
                        con.Cmd.Parameters.AddWithValue("@ID", ID);
                        con.ExecuteCRUDQuery();
                        UpdateUI();
                        orgName.Text = "";
                        orgAdd.Text = "";
                        Status.Text = "Record Deleted";
                        helpers.ShowQueryStatus(Status, "Record Deleted");
                    }
                    catch (Exception)
                    {
                        helpers.ShowQueryStatus(Status, "This value can't be deleted, it is used elsewhere", "Err");
                    }
                }
                else
                {
                    OrgDGV.ClearSelection();
                }
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Select a Row to Delete", "Err");
            }

        }

        
    }
}
