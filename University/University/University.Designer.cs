﻿namespace University
{
    partial class University
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.OrganizationDB = new System.Windows.Forms.Button();
            this.GuestsDB = new System.Windows.Forms.Button();
            this.LectursDB = new System.Windows.Forms.Button();
            this.ComplexQueryDB = new System.Windows.Forms.Button();
            this.LecturerDB = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Perpetua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 66);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(80, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "University Management Kit";
            // 
            // OrganizationDB
            // 
            this.OrganizationDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrganizationDB.Location = new System.Drawing.Point(26, 150);
            this.OrganizationDB.Margin = new System.Windows.Forms.Padding(4);
            this.OrganizationDB.Name = "OrganizationDB";
            this.OrganizationDB.Size = new System.Drawing.Size(159, 76);
            this.OrganizationDB.TabIndex = 1;
            this.OrganizationDB.Text = "Organization";
            this.OrganizationDB.UseVisualStyleBackColor = true;
            this.OrganizationDB.Click += new System.EventHandler(this.OrganizationDB_Click);
            // 
            // GuestsDB
            // 
            this.GuestsDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GuestsDB.Location = new System.Drawing.Point(26, 279);
            this.GuestsDB.Margin = new System.Windows.Forms.Padding(4);
            this.GuestsDB.Name = "GuestsDB";
            this.GuestsDB.Size = new System.Drawing.Size(159, 76);
            this.GuestsDB.TabIndex = 4;
            this.GuestsDB.Text = "Guests";
            this.GuestsDB.UseVisualStyleBackColor = true;
            this.GuestsDB.Click += new System.EventHandler(this.GuestsDB_Click);
            // 
            // LectursDB
            // 
            this.LectursDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LectursDB.Location = new System.Drawing.Point(335, 150);
            this.LectursDB.Margin = new System.Windows.Forms.Padding(4);
            this.LectursDB.Name = "LectursDB";
            this.LectursDB.Size = new System.Drawing.Size(159, 76);
            this.LectursDB.TabIndex = 2;
            this.LectursDB.Text = "Lecture";
            this.LectursDB.UseVisualStyleBackColor = true;
            this.LectursDB.Click += new System.EventHandler(this.LectursDB_Click);
            // 
            // ComplexQueryDB
            // 
            this.ComplexQueryDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComplexQueryDB.Location = new System.Drawing.Point(335, 279);
            this.ComplexQueryDB.Margin = new System.Windows.Forms.Padding(4);
            this.ComplexQueryDB.Name = "ComplexQueryDB";
            this.ComplexQueryDB.Size = new System.Drawing.Size(159, 76);
            this.ComplexQueryDB.TabIndex = 5;
            this.ComplexQueryDB.Text = "Complex Query?";
            this.ComplexQueryDB.UseVisualStyleBackColor = true;
            this.ComplexQueryDB.Click += new System.EventHandler(this.ComplexQueryDB_Click);
            // 
            // LecturerDB
            // 
            this.LecturerDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LecturerDB.Location = new System.Drawing.Point(616, 150);
            this.LecturerDB.Margin = new System.Windows.Forms.Padding(4);
            this.LecturerDB.Name = "LecturerDB";
            this.LecturerDB.Size = new System.Drawing.Size(159, 76);
            this.LecturerDB.TabIndex = 3;
            this.LecturerDB.Text = "Lecturer";
            this.LecturerDB.UseVisualStyleBackColor = true;
            this.LecturerDB.Click += new System.EventHandler(this.LecturerDB_Click);
            // 
            // University
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LecturerDB);
            this.Controls.Add(this.ComplexQueryDB);
            this.Controls.Add(this.LectursDB);
            this.Controls.Add(this.GuestsDB);
            this.Controls.Add(this.OrganizationDB);
            this.Controls.Add(this.panel1);
            this.Name = "University";
            this.Text = "University Management";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OrganizationDB;
        private System.Windows.Forms.Button GuestsDB;
        private System.Windows.Forms.Button LectursDB;
        private System.Windows.Forms.Button ComplexQueryDB;
        private System.Windows.Forms.Button LecturerDB;
    }
}