﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace University
{
    public partial class LecturersForm : Form
    {
        Helpers helpers = new Helpers();
        private SqlDbConnect con;
        int index = 0;

        public LecturersForm()
        {
            con = new SqlDbConnect();
            InitializeComponent();
            UpdateUI();
        }

        private void UpdateUI() {
            LecturerDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            LecturerDGV.MultiSelect = false;

            helpers.PopulateSelectBoxes(Speciality, "SELECT Lec_ID, Subject FROM Lectures");
            helpers.PopulateSelectBoxes(WorkPlace, "SELECT Org_ID, Org_name FROM Organization");
            string queryString = @"SELECT l.Lect_ID, l.Lect_name, lec.Subject, o.Org_name 
                                    FROM Lecturers l 
                                    INNER JOIN Organization o on l.Org_ID = o.Org_ID 
                                    INNER JOIN Lectures lec on l.Lec_ID = lec.Lec_ID";
            con.SqlQuery(queryString);
            LecturerDGV.AutoGenerateColumns = false;
            var lecturers = con.ExecuteReturnQuery();
            LecturerDGV.DataSource = lecturers;
            LecturerDGV.ClearSelection();
        }

        private void ExportResult_Click(object sender, EventArgs e)
        {
            helpers.ExportResults(LecturerDGV);
        }

        private void LecturerDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.LecturerDGV.Rows[e.RowIndex];
                lecturerID.Text = row.Cells["Lect_ID"].Value.ToString().Trim();
                lecturerName.Text = row.Cells["Lect_name"].Value.ToString().Trim();
                Speciality.SelectedIndex = Speciality.FindStringExact(row.Cells["Subject"].Value.ToString().Trim());
                WorkPlace.SelectedIndex = WorkPlace.FindStringExact(row.Cells["Org_name"].Value.ToString().Trim());
            }
        }

        private void CloseUI_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Add_Click_1(object sender, EventArgs e)
        {
            string lecturername = lecturerName.Text.Trim();
            string selectedSpeciality = ((ComboBoxItem)Speciality.SelectedItem).HiddenValue;
            string selectedWorkplace = ((ComboBoxItem)WorkPlace.SelectedItem).HiddenValue;

            if (!string.IsNullOrEmpty(lecturername) && !string.IsNullOrEmpty(selectedSpeciality) && !string.IsNullOrEmpty(selectedWorkplace))
            {
                con.SqlQuery("INSERT INTO Lecturers (Lect_name, Subject, Workingplace) VALUES (@name, @subject, @workplace)");
                con.Cmd.Parameters.AddWithValue("@name", lecturername);
                con.Cmd.Parameters.AddWithValue("@subject", selectedSpeciality);
                con.Cmd.Parameters.AddWithValue("@workplace", selectedWorkplace);
                con.ExecuteCRUDQuery();
                UpdateUI();
                lecturerName.Text = "";
                helpers.ShowQueryStatus(Status, "Data saved");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void UpdateEntry_Click_1(object sender, EventArgs e)
        {
            int ID = Int32.Parse(lecturerID.Text.Trim());
            string lecturername = lecturerName.Text.Trim();
            string selectedSpeciality = ((ComboBoxItem)Speciality.SelectedItem).HiddenValue;
            string selectedWorkplace = ((ComboBoxItem)WorkPlace.SelectedItem).HiddenValue;

            if (!string.IsNullOrEmpty(lecturername) && !string.IsNullOrEmpty(selectedSpeciality) && !string.IsNullOrEmpty(selectedWorkplace))
            {
                con.SqlQuery("UPDATE Lecturers SET Lect_name = @name, Subject = @subject, Workingplace = @workplace WHERE Lect_ID = @ID");
                con.Cmd.Parameters.AddWithValue("@name", lecturername);
                con.Cmd.Parameters.AddWithValue("@subject", selectedSpeciality);
                con.Cmd.Parameters.AddWithValue("@workplace", selectedWorkplace);
                con.Cmd.Parameters.AddWithValue("@ID", ID);
                con.ExecuteCRUDQuery();
                UpdateUI();
                lecturerName.Text = "";
                helpers.ShowQueryStatus(Status, "Record Updated");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void deleteLect_Click_1(object sender, EventArgs e)
        {

            if (LecturerDGV.SelectedRows.Count > 0)
            {
                var confirmResult = MessageBox.Show("Are you sure to delete this item ??", "Confirm Delete!!", MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    int ID = Int32.Parse(lecturerID.Text.Trim());
                    try
                    {
                        con.SqlQuery("DELETE FROM Lecturers WHERE Lect_ID = @ID");
                        con.Cmd.Parameters.AddWithValue("@ID", ID);
                        con.ExecuteCRUDQuery();
                        UpdateUI();
                        lecturerName.Text = "";
                        Status.Text = "Record Deleted";
                        helpers.ShowQueryStatus(Status, "Record Deleted");
                    }
                    catch (Exception)
                    {
                        helpers.ShowQueryStatus(Status, "This value can't be deleted, it is used elsewhere", "Err");
                    }
                }
                else
                {
                    LecturerDGV.ClearSelection();
                }
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Select a Row to Delete", "Err");
            }
        }
    }
}
