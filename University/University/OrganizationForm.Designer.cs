﻿namespace University
{
    partial class OrganizationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseUI = new System.Windows.Forms.Button();
            this.ExportResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orgUpdate = new System.Windows.Forms.Button();
            this.orgDel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.queryStatus = new System.Windows.Forms.Label();
            this.orgNew = new System.Windows.Forms.Button();
            this.orgName = new System.Windows.Forms.TextBox();
            this.orgAdd = new System.Windows.Forms.TextBox();
            this.formOrgID = new System.Windows.Forms.TextBox();
            this.OrgDGV = new System.Windows.Forms.DataGridView();
            this.Org_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Org_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrgDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.CloseUI);
            this.panel1.Controls.Add(this.ExportResult);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Perpetua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1349, 66);
            this.panel1.TabIndex = 2;
            // 
            // CloseUI
            // 
            this.CloseUI.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseUI.Location = new System.Drawing.Point(1140, 11);
            this.CloseUI.Name = "CloseUI";
            this.CloseUI.Size = new System.Drawing.Size(141, 48);
            this.CloseUI.TabIndex = 7;
            this.CloseUI.Text = "Close Window";
            this.CloseUI.UseVisualStyleBackColor = true;
            this.CloseUI.Click += new System.EventHandler(this.CloseUI_Click);
            // 
            // ExportResult
            // 
            this.ExportResult.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportResult.Location = new System.Drawing.Point(809, 11);
            this.ExportResult.Name = "ExportResult";
            this.ExportResult.Size = new System.Drawing.Size(141, 48);
            this.ExportResult.TabIndex = 6;
            this.ExportResult.Text = "Export Results";
            this.ExportResult.UseVisualStyleBackColor = true;
            this.ExportResult.Click += new System.EventHandler(this.ExportResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "University Management Kit";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.orgUpdate);
            this.groupBox1.Controls.Add(this.orgDel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.queryStatus);
            this.groupBox1.Controls.Add(this.orgNew);
            this.groupBox1.Controls.Add(this.orgName);
            this.groupBox1.Controls.Add(this.orgAdd);
            this.groupBox1.Controls.Add(this.formOrgID);
            this.groupBox1.Location = new System.Drawing.Point(642, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(641, 486);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // orgUpdate
            // 
            this.orgUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orgUpdate.Location = new System.Drawing.Point(218, 363);
            this.orgUpdate.Name = "orgUpdate";
            this.orgUpdate.Size = new System.Drawing.Size(145, 55);
            this.orgUpdate.TabIndex = 4;
            this.orgUpdate.Text = "Update";
            this.orgUpdate.UseVisualStyleBackColor = true;
            this.orgUpdate.Click += new System.EventHandler(this.orgUpdate_Click);
            // 
            // orgDel
            // 
            this.orgDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orgDel.Location = new System.Drawing.Point(405, 363);
            this.orgDel.Name = "orgDel";
            this.orgDel.Size = new System.Drawing.Size(145, 55);
            this.orgDel.TabIndex = 5;
            this.orgDel.Text = "Delete";
            this.orgDel.UseVisualStyleBackColor = true;
            this.orgDel.Click += new System.EventHandler(this.orgDel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Organization Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(41, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Address";
            // 
            // queryStatus
            // 
            this.queryStatus.AutoSize = true;
            this.queryStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.queryStatus.Location = new System.Drawing.Point(61, 26);
            this.queryStatus.Name = "queryStatus";
            this.queryStatus.Size = new System.Drawing.Size(0, 20);
            this.queryStatus.TabIndex = 4;
            // 
            // orgNew
            // 
            this.orgNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orgNew.Location = new System.Drawing.Point(36, 363);
            this.orgNew.Name = "orgNew";
            this.orgNew.Size = new System.Drawing.Size(145, 55);
            this.orgNew.TabIndex = 3;
            this.orgNew.Text = "Add";
            this.orgNew.UseVisualStyleBackColor = true;
            this.orgNew.Click += new System.EventHandler(this.orgNew_Click);
            // 
            // orgName
            // 
            this.orgName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orgName.Location = new System.Drawing.Point(243, 123);
            this.orgName.Name = "orgName";
            this.orgName.Size = new System.Drawing.Size(336, 26);
            this.orgName.TabIndex = 1;
            // 
            // orgAdd
            // 
            this.orgAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orgAdd.Location = new System.Drawing.Point(243, 166);
            this.orgAdd.Name = "orgAdd";
            this.orgAdd.Size = new System.Drawing.Size(336, 26);
            this.orgAdd.TabIndex = 2;
            // 
            // formOrgID
            // 
            this.formOrgID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formOrgID.Location = new System.Drawing.Point(243, 80);
            this.formOrgID.Name = "formOrgID";
            this.formOrgID.Size = new System.Drawing.Size(336, 26);
            this.formOrgID.TabIndex = 0;
            this.formOrgID.Visible = false;
            // 
            // OrgDGV
            // 
            this.OrgDGV.AllowUserToAddRows = false;
            this.OrgDGV.AllowUserToDeleteRows = false;
            this.OrgDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.OrgDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrgDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Org_ID,
            this.Org_name,
            this.Address});
            this.OrgDGV.Location = new System.Drawing.Point(40, 100);
            this.OrgDGV.Name = "OrgDGV";
            this.OrgDGV.Size = new System.Drawing.Size(555, 505);
            this.OrgDGV.TabIndex = 9;
            this.OrgDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OrgDGV_CellMouseClick);
            // 
            // Org_ID
            // 
            this.Org_ID.DataPropertyName = "Org_ID";
            this.Org_ID.HeaderText = "ID";
            this.Org_ID.Name = "Org_ID";
            this.Org_ID.ReadOnly = true;
            this.Org_ID.Visible = false;
            // 
            // Org_name
            // 
            this.Org_name.DataPropertyName = "Org_name";
            this.Org_name.HeaderText = "Organization Name";
            this.Org_name.Name = "Org_name";
            this.Org_name.ReadOnly = true;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address";
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(45, 20);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 25);
            this.Status.TabIndex = 9;
            // 
            // OrganizationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1349, 640);
            this.Controls.Add(this.OrgDGV);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "OrganizationForm";
            this.Text = "OrganizationForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrgDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseUI;
        private System.Windows.Forms.Button ExportResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button orgUpdate;
        private System.Windows.Forms.Button orgDel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label queryStatus;
        private System.Windows.Forms.Button orgNew;
        private System.Windows.Forms.TextBox orgName;
        private System.Windows.Forms.TextBox orgAdd;
        private System.Windows.Forms.TextBox formOrgID;
        private System.Windows.Forms.DataGridView OrgDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Org_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Org_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.Label Status;
    }
}