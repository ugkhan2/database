﻿using System;
using System.Windows.Forms;

namespace University
{
    public partial class University : Form
    {
        Helpers helper = new Helpers();       
        
        //LecturersForm lecturers = new LecturersForm();
        //OrganizationForm organization = new OrganizationForm();
        //OrganizationForm organization = new OrganizationForm();

        public University()
        {
            InitializeComponent();
        }

        private void OrganizationDB_Click(object sender, EventArgs e)
        {
            OrganizationForm organization = new OrganizationForm();
            organization.Show();
            organization.Text = "Organizations";
        }

        private void LecturerDB_Click(object sender, EventArgs e)
        {
            LecturersForm lecturers = new LecturersForm();
            lecturers.Show();
            lecturers.Text = "Lecturers";
        }

        private void LectursDB_Click(object sender, EventArgs e)
        {
            LecturesForm lectures = new LecturesForm();
            lectures.Show();
        }

        private void GuestsDB_Click(object sender, EventArgs e)
        {
            GuestForm guests = new GuestForm();
            guests.Show();
            guests.Text = "Guests";
        }

        private void ComplexQueryDB_Click(object sender, EventArgs e)
        {
            ComplexQuery complexQuery = new ComplexQuery();
            complexQuery.Show();
            complexQuery.Text = "Execute Complex Queries";
        }
    }
}
