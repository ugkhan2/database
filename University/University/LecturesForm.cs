﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace University
{
    public partial class LecturesForm : Form
    {
        Helpers helpers = new Helpers();
        private SqlDbConnect con;

        public LecturesForm()
        {
            con = new SqlDbConnect();
            InitializeComponent();
            UpdateUI();
            this.Text = "Lectures";
        }

        private void UpdateUI() {
            LecturesDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            LecturesDGV.MultiSelect = false;
            con.SqlQuery("SELECT * FROM Lectures");
            var Guests = con.ExecuteReturnQuery();
            LecturesDGV.AutoGenerateColumns = false;
            LecturesDGV.DataSource = Guests;
            LecturesDGV.ClearSelection();
        }

        private void LecturesDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.LecturesDGV.Rows[e.RowIndex];
                formLecID.Text = row.Cells["Lec_ID"].Value.ToString().Trim();
                SubjectName.Text = row.Cells["Subject"].Value.ToString().Trim();
                lecDate.Text = row.Cells["Date"].Value.ToString().Trim();
                lecDuration.Text = row.Cells["Duration"].Value.ToString().Trim();
            }
        }

        private void CloseUI_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ExportResult_Click(object sender, EventArgs e)
        {
            helpers.ExportResults(LecturesDGV);
        }

        private void lecNew_Click(object sender, EventArgs e)
        {
            string subject = SubjectName.Text.Trim();
            string lectureDate = lecDate.Text.Trim();
            string lectureDuration = lecDuration.Text.Trim();

            if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(lectureDate) && !string.IsNullOrEmpty(lectureDuration))
            {
                con.SqlQuery("INSERT INTO Lectures (Subject, Date, Duration) VALUES (@subject, @date, @duration)");
                con.Cmd.Parameters.AddWithValue("@subject", subject);
                con.Cmd.Parameters.AddWithValue("@date", lectureDate);
                con.Cmd.Parameters.AddWithValue("@duration", lectureDuration);
                con.ExecuteCRUDQuery();
                UpdateUI();
                SubjectName.Text = "";
                lecDate.Text = "";
                lecDuration.Text = "";
                helpers.ShowQueryStatus(Status, "Data saved");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void lecUpdate_Click(object sender, EventArgs e)
        {
            string subject = SubjectName.Text.Trim();
            string ID = formLecID.Text.Trim();
            string lectureDate = lecDate.Text.Trim();
            string lectureDuration = lecDuration.Text.Trim();

            if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(lectureDate) && !string.IsNullOrEmpty(lectureDuration))
            {
                con.SqlQuery("UPDATE Lectures SET Subject = @subject, Date = @date, Duration = @duration WHERE Lec_ID =  @ID");
                con.Cmd.Parameters.AddWithValue("@subject", subject);
                con.Cmd.Parameters.AddWithValue("@date", lectureDate);
                con.Cmd.Parameters.AddWithValue("@ID", ID);
                con.Cmd.Parameters.AddWithValue("@duration", lectureDuration);
                con.ExecuteCRUDQuery();
                UpdateUI();
                SubjectName.Text = "";
                lecDate.Text = "";
                lecDuration.Text = "";
                helpers.ShowQueryStatus(Status, "Data Update");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void lecDel_Click(object sender, EventArgs e)
        {
            if (LecturesDGV.SelectedRows.Count > 0)
            {
                var confirmResult = MessageBox.Show("Are you sure to delete this item ??", "Confirm Delete!!", MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    int ID = Int32.Parse(formLecID.Text.Trim());
                    try
                    {
                        con.SqlQuery("DELETE FROM Lectures WHERE Lec_ID = @ID");
                        con.Cmd.Parameters.AddWithValue("@ID", ID);
                        con.ExecuteCRUDQuery();
                        UpdateUI();
                        SubjectName.Text = "";
                        lecDate.Text = "";
                        lecDuration.Text = "";
                        helpers.ShowQueryStatus(Status, "Record Deleted");
                    }
                    catch (Exception)
                    {
                        helpers.ShowQueryStatus(Status, "This value can't be deleted, it is used elsewhere", "Err");
                    }
                }
                else
                {
                    LecturesDGV.ClearSelection();
                }
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Select a Row to Delete", "Err");
            }
        }
    }
}
