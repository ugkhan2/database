﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace University
{
    public partial class GuestForm : Form
    {
        Helpers helpers = new Helpers();
        private SqlDbConnect con;
        int index = 0;

        public GuestForm()
        {
            con = new SqlDbConnect();
            InitializeComponent();
            UpdateUI();
        }
        private void UpdateUI()
        {
            GuestDGV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GuestDGV.MultiSelect = false;
            helpers.PopulateSelectBoxes(lecID, "SELECT Lec_ID, Subject FROM Lectures");
            helpers.PopulateSelectBoxes(orgID, "SELECT Org_ID, Org_name FROM Organization");
            helpers.PopulateSelectBoxes(lectID, "SELECT Lect_ID, Lect_name FROM lecturers");
            string queryString = @"SELECT g.Guest_ID, o.Org_name, lec.Subject, lect.Lect_name, g.Date, g.Time 
                                FROM Guest g 
                                INNER JOIN Organization o on g.Org_ID = o.Org_ID 
                                INNER JOIN Lecturers lect on g.Lect_ID = lect.Lect_ID
                                INNER JOIN Lectures lec on g.Lec_ID = lec.Lec_ID";
            con.SqlQuery(queryString);
            GuestDGV.AutoGenerateColumns = false;
            var Guests = con.ExecuteReturnQuery();
            GuestDGV.DataSource = Guests;
            GuestDGV.ClearSelection();
        }

        private void GuestDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.GuestDGV.Rows[e.RowIndex];
                guestID.Text = row.Cells["Guest_ID"].Value.ToString().Trim();
                orgID.SelectedIndex = orgID.FindStringExact(row.Cells["Org_name"].Value.ToString().Trim());
                lecID.SelectedIndex = lecID.FindStringExact(row.Cells["Subject"].Value.ToString().Trim());
                lectID.SelectedIndex = lectID.FindStringExact(row.Cells["Lect_name"].Value.ToString().Trim());
                duration.Text = row.Cells["Time"].Value.ToString().Trim();
                datenTime.Text = row.Cells["Date"].Value.ToString().Trim();
            }
        }

        private void AddGuest_Click(object sender, EventArgs e)
        {
            string date = datenTime.Text.Trim();
            int durationt = Int32.Parse(duration.Text.Trim());
            string organizationID = ((ComboBoxItem)orgID.SelectedItem).HiddenValue;
            string lectureID = ((ComboBoxItem)lecID.SelectedItem).HiddenValue;
            string lecturerID = ((ComboBoxItem)lectID.SelectedItem).HiddenValue;

            if (!string.IsNullOrEmpty(date) &&
                !string.IsNullOrEmpty(organizationID) &&
                !string.IsNullOrEmpty(lectureID) &&
                !string.IsNullOrEmpty(lecturerID))
            {
                con.SqlQuery("INSERT INTO Guest (Org_ID, Lec_ID, Lect_ID, Date, Time) VALUES (@orgID, @lecID, @lectID, @date, @time)");
                con.Cmd.Parameters.AddWithValue("@orgID", organizationID);
                con.Cmd.Parameters.AddWithValue("@lecID", lectureID);
                con.Cmd.Parameters.AddWithValue("@lectID", lecturerID);
                con.Cmd.Parameters.AddWithValue("@date", date);
                con.Cmd.Parameters.AddWithValue("@time", durationt);
                con.ExecuteCRUDQuery();
                UpdateUI();
                duration.Text = "";
                datenTime.Text = "";
                helpers.ShowQueryStatus(Status, "Data saved");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }
        }

        private void UpdateGuest_Click(object sender, EventArgs e)
        {
            int ID = Int32.Parse(guestID.Text.Trim());
            string dateStr = datenTime.Text.Trim();
            //DateTime date = DateTime.Parse(dateStr);
            int durationt = Int32.Parse(duration.Text.Trim());
            string organizationID = ((ComboBoxItem)orgID.SelectedItem).HiddenValue;
            string lectureID = ((ComboBoxItem)lecID.SelectedItem).HiddenValue;
            string lecturerID = ((ComboBoxItem)lectID.SelectedItem).HiddenValue;

            if (!string.IsNullOrEmpty(dateStr) &&
                !string.IsNullOrEmpty(organizationID) &&
                !string.IsNullOrEmpty(lectureID) &&
                !string.IsNullOrEmpty(lecturerID))
                {
                con.SqlQuery("UPDATE Guest SET " +
                    "Org_ID = @orgID, "+
                    "lec_ID = @lecID, " +
                    "Lect_ID = @lectID, " +
                    "Date = @date, " +
                    "Time = @time " +
                    "WHERE Guest_ID = @ID");
                con.Cmd.Parameters.AddWithValue("@orgID", organizationID);
                con.Cmd.Parameters.AddWithValue("@lecID", lectureID);
                con.Cmd.Parameters.AddWithValue("@lectID", lecturerID);
                con.Cmd.Parameters.AddWithValue("@date", dateStr);
                con.Cmd.Parameters.AddWithValue("@time", durationt);
                con.Cmd.Parameters.AddWithValue("@ID", ID);
                con.ExecuteCRUDQuery();
                UpdateUI();
                duration.Text = "";
                datenTime.Text = "";
                helpers.ShowQueryStatus(Status, "Record Updated");
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Some of your Data is missing", "Err");
            }

        }

        private void DeleteGuest_Click(object sender, EventArgs e)
        {

            if (GuestDGV.SelectedRows.Count > 0)
            {
                if (helpers.DeleteAlertBox())
                {
                    int ID = Int32.Parse(guestID.Text.Trim());
                    try
                    {
                        con.SqlQuery("DELETE FROM Guest WHERE Guest_ID = @ID");
                        con.Cmd.Parameters.AddWithValue("@ID", ID);
                        con.ExecuteCRUDQuery();
                        UpdateUI();
                        datenTime.Text = "";
                        duration.Text = "";
                        helpers.ShowQueryStatus(Status, "Record Deleted");
                    }
                    catch (Exception)
                    {
                        helpers.ShowQueryStatus(Status, "This value can't be deleted, it is used elsewhere", "Err");
                    }
                }
                else
                {
                    GuestDGV.ClearSelection();
                }
            }
            else
            {
                helpers.ShowQueryStatus(Status, "Select a Row to Delete", "Err");
            }

    }

        private void CloseUI_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ExportResult_Click(object sender, EventArgs e)
        {
            helpers.ExportResults(GuestDGV);
        }
    }
}
