﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace University
{
    public partial class ComplexQuery : Form
    {
        Helpers helpers = new Helpers();
        private SqlDbConnect con;
        int index = 0;
        public ComplexQuery()
        {
            con = new SqlDbConnect();
            InitializeComponent();
            UpdateUI();
        }

        private void CloseUI_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdateUI() {
        }

        private void ExportResult_Click(object sender, EventArgs e)
        {
            helpers.ExportResults(ComplexDGV);
        }

        private void runQuery_Click(object sender, EventArgs e)
        {
            var complexQuery = query.Text.Trim();
            if(!string.IsNullOrEmpty(complexQuery))
            {
                try
                {
                    //ExampleQuery: SELECT* FROM GUEST g, Organization o WHERE g.Org_ID = o.Org_ID
                    con.SqlQuery(complexQuery);
                    var result = con.ExecuteReturnQuery();
                    ComplexDGV.DataSource = result;
                    helpers.ShowQueryStatus(Status, "Query Completed");
                }
                catch (Exception ex)
                {
                    helpers.ShowQueryStatus(Status, ex.Message, "Err");
                }

            }
            else
            {
                helpers.ShowQueryStatus(Status, "Please write your query first", "Err");
            }
        }
    }
}
