﻿namespace University
{
    partial class LecturersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LecturerDGV = new System.Windows.Forms.DataGridView();
            this.LecturerForm = new System.Windows.Forms.GroupBox();
            this.deleteLect = new System.Windows.Forms.Button();
            this.lecturerID = new System.Windows.Forms.TextBox();
            this.Status = new System.Windows.Forms.Label();
            this.WorkPlace = new System.Windows.Forms.ComboBox();
            this.Speciality = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lecturerName = new System.Windows.Forms.TextBox();
            this.UpdateEntry = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseUI = new System.Windows.Forms.Button();
            this.ExportResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Lect_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lect_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Org_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.LecturerDGV)).BeginInit();
            this.LecturerForm.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LecturerDGV
            // 
            this.LecturerDGV.AllowUserToAddRows = false;
            this.LecturerDGV.AllowUserToDeleteRows = false;
            this.LecturerDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.LecturerDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LecturerDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Lect_ID,
            this.Lect_name,
            this.Subject,
            this.Org_name});
            this.LecturerDGV.Location = new System.Drawing.Point(1, 73);
            this.LecturerDGV.Name = "LecturerDGV";
            this.LecturerDGV.ReadOnly = true;
            this.LecturerDGV.Size = new System.Drawing.Size(634, 572);
            this.LecturerDGV.TabIndex = 1;
            this.LecturerDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.LecturerDGV_CellMouseClick);
            // 
            // LecturerForm
            // 
            this.LecturerForm.Controls.Add(this.deleteLect);
            this.LecturerForm.Controls.Add(this.lecturerID);
            this.LecturerForm.Controls.Add(this.Status);
            this.LecturerForm.Controls.Add(this.WorkPlace);
            this.LecturerForm.Controls.Add(this.Speciality);
            this.LecturerForm.Controls.Add(this.label5);
            this.LecturerForm.Controls.Add(this.label4);
            this.LecturerForm.Controls.Add(this.label2);
            this.LecturerForm.Controls.Add(this.lecturerName);
            this.LecturerForm.Controls.Add(this.UpdateEntry);
            this.LecturerForm.Controls.Add(this.Add);
            this.LecturerForm.Location = new System.Drawing.Point(685, 73);
            this.LecturerForm.Name = "LecturerForm";
            this.LecturerForm.Size = new System.Drawing.Size(646, 483);
            this.LecturerForm.TabIndex = 2;
            this.LecturerForm.TabStop = false;
            // 
            // deleteLect
            // 
            this.deleteLect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteLect.Location = new System.Drawing.Point(488, 381);
            this.deleteLect.Name = "deleteLect";
            this.deleteLect.Size = new System.Drawing.Size(138, 71);
            this.deleteLect.TabIndex = 8;
            this.deleteLect.Text = "Delete";
            this.deleteLect.UseVisualStyleBackColor = true;
            this.deleteLect.Click += new System.EventHandler(this.deleteLect_Click_1);
            // 
            // lecturerID
            // 
            this.lecturerID.Location = new System.Drawing.Point(189, 80);
            this.lecturerID.Name = "lecturerID";
            this.lecturerID.Size = new System.Drawing.Size(100, 20);
            this.lecturerID.TabIndex = 12;
            this.lecturerID.Visible = false;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(30, 25);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 25);
            this.Status.TabIndex = 3;
            // 
            // WorkPlace
            // 
            this.WorkPlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WorkPlace.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.WorkPlace.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkPlace.FormattingEnabled = true;
            this.WorkPlace.Location = new System.Drawing.Point(189, 227);
            this.WorkPlace.Name = "WorkPlace";
            this.WorkPlace.Size = new System.Drawing.Size(438, 26);
            this.WorkPlace.TabIndex = 5;
            // 
            // Speciality
            // 
            this.Speciality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Speciality.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Speciality.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Speciality.FormattingEnabled = true;
            this.Speciality.Location = new System.Drawing.Point(189, 179);
            this.Speciality.Name = "Speciality";
            this.Speciality.Size = new System.Drawing.Size(438, 26);
            this.Speciality.Sorted = true;
            this.Speciality.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(50, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Speciality";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(50, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Work Place";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(50, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Lecturers Name";
            // 
            // lecturerName
            // 
            this.lecturerName.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturerName.Location = new System.Drawing.Point(189, 127);
            this.lecturerName.Name = "lecturerName";
            this.lecturerName.Size = new System.Drawing.Size(438, 26);
            this.lecturerName.TabIndex = 3;
            // 
            // UpdateEntry
            // 
            this.UpdateEntry.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateEntry.Location = new System.Drawing.Point(305, 381);
            this.UpdateEntry.Name = "UpdateEntry";
            this.UpdateEntry.Size = new System.Drawing.Size(138, 71);
            this.UpdateEntry.TabIndex = 7;
            this.UpdateEntry.Text = "Update";
            this.UpdateEntry.UseVisualStyleBackColor = true;
            this.UpdateEntry.Click += new System.EventHandler(this.UpdateEntry_Click_1);
            // 
            // Add
            // 
            this.Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add.Location = new System.Drawing.Point(122, 381);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(138, 71);
            this.Add.TabIndex = 6;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click_1);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.CloseUI);
            this.panel1.Controls.Add(this.ExportResult);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Perpetua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1349, 66);
            this.panel1.TabIndex = 3;
            // 
            // CloseUI
            // 
            this.CloseUI.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseUI.Location = new System.Drawing.Point(1140, 11);
            this.CloseUI.Name = "CloseUI";
            this.CloseUI.Size = new System.Drawing.Size(141, 48);
            this.CloseUI.TabIndex = 2;
            this.CloseUI.Text = "Close Window";
            this.CloseUI.UseVisualStyleBackColor = true;
            this.CloseUI.Click += new System.EventHandler(this.CloseUI_Click);
            // 
            // ExportResult
            // 
            this.ExportResult.Font = new System.Drawing.Font("Perpetua", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportResult.Location = new System.Drawing.Point(809, 11);
            this.ExportResult.Name = "ExportResult";
            this.ExportResult.Size = new System.Drawing.Size(141, 48);
            this.ExportResult.TabIndex = 1;
            this.ExportResult.Text = "Export Results";
            this.ExportResult.UseVisualStyleBackColor = true;
            this.ExportResult.Click += new System.EventHandler(this.ExportResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "University Management Kit";
            // 
            // Lect_ID
            // 
            this.Lect_ID.DataPropertyName = "Lect_ID";
            this.Lect_ID.HeaderText = "ID";
            this.Lect_ID.Name = "Lect_ID";
            this.Lect_ID.ReadOnly = true;
            this.Lect_ID.Visible = false;
            // 
            // Lect_name
            // 
            this.Lect_name.DataPropertyName = "Lect_name";
            this.Lect_name.HeaderText = "Lecturers Name";
            this.Lect_name.Name = "Lect_name";
            this.Lect_name.ReadOnly = true;
            // 
            // Subject
            // 
            this.Subject.DataPropertyName = "Subject";
            this.Subject.HeaderText = "Speciality";
            this.Subject.Name = "Subject";
            this.Subject.ReadOnly = true;
            // 
            // Org_name
            // 
            this.Org_name.DataPropertyName = "Org_name";
            this.Org_name.HeaderText = "Work Place";
            this.Org_name.Name = "Org_name";
            this.Org_name.ReadOnly = true;
            // 
            // LecturersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 646);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LecturerForm);
            this.Controls.Add(this.LecturerDGV);
            this.Name = "LecturersForm";
            this.Text = "LecturersForm";
            ((System.ComponentModel.ISupportInitialize)(this.LecturerDGV)).EndInit();
            this.LecturerForm.ResumeLayout(false);
            this.LecturerForm.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView LecturerDGV;
        private System.Windows.Forms.GroupBox LecturerForm;
        private System.Windows.Forms.Button deleteLect;
        private System.Windows.Forms.TextBox lecturerID;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.ComboBox WorkPlace;
        private System.Windows.Forms.ComboBox Speciality;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lecturerName;
        private System.Windows.Forms.Button UpdateEntry;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CloseUI;
        private System.Windows.Forms.Button ExportResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lect_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lect_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn Org_name;
    }
}